export function sum(...numbers: number[]): number {
    return numbers.reduce((acc: number, curr: number): number => {
        return acc + curr;
    }, 0);
}

export function multiply(multiplier: number, ...numbers: number[]): number[] {
    return numbers.map((num) => multiplier * num);
}
