import { expect } from 'chai';
import { sum, multiply } from "../src/calc";

describe("tests of sum function", () => {
    context("calc module", () => {

        it("should be a function", () => {
            expect(sum).to.be.a('function');
            expect(sum).to.be.a.instanceOf(Function);
        }),

        it("should sum up two numbers", () => {
            expect(sum(5, 10)).to.equal(15);
        })

        it("should sum any number of numbers", () => {
            expect(sum(5, 5, 5, 5, 10, 10 ,10, 50)).to.equal(100);
        })
    })
})

describe("tests of multiply function", () => {
    context("calc module", () => {

        it("should be a function", () => {
            expect(multiply).to.be.a('function');
            expect(multiply).to.be.a.instanceOf(Function);
        }),

        it("should multiple one number with multiplier", () => {
            expect(multiply(5, 10)).to.deep.equal([50]);
        })

        it("should multilple any number of numbers with multiplier", () => {
            expect(multiply(2, 1, 2, 3, 4, 5)).to.deep.equal([2,4,6,8,10]);
        })
    })
})

describe("async tests of multiply function", () => {
    context("calc module", () => {

        it("async - should multiple one number with multiplier", async () => {
            await delay(1000);
            expect(multiply(5, 10)).to.deep.equal([50]);
        })

        it("async - should multilple any number of numbers with multiplier", async () => {
            await delay(1000);
            expect(multiply(2, 1, 2, 3, 4, 5)).to.deep.equal([2,4,6,8,10]);
        })
    })
})

function delay(ms: number): Promise<string> {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve('resolved');
      }, ms);
    });
  }